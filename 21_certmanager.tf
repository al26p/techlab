resource "helm_release" "cert_manager" {
  name = "ingress-nginx"

  repository       = "https://charts.jetstack.io"
  chart            = "cert-manager"
  namespace        = "cert-manager"
  version          = "1.13.2"
  create_namespace = true

  values = [
    "installCRDs: true"
  ]

  depends_on = [ovh_cloud_project_kube_nodepool.node_pool]
}

resource "helm_release" "ovh_webhook" {
  name  = "cert-manager-webhook-ovh"
  chart = "charts/ovh_certmanager"

  depends_on = [helm_release.cert_manager]
}

resource "kubernetes_secret" "ovh_credentials" {
  metadata {
    name      = "ovh-credentials"
    namespace = "cert-manager"
  }

  data = {
    applicationKey    = var.DNS_OVH_APPLICATION_KEY
    applicationSecret = var.DNS_OVH_APPLICATION_SECRET
    consumerKey       = var.DNS_OVH_CONSUMER_KEY
  }
}

resource "kubernetes_cluster_role" "cm_webhook_role" {
  metadata {
    name = "cert-manager-webhook-ovh:secret-reader"
    # namespace = "cert-manager"
  }

  rule {
    api_groups     = [""]
    resources      = ["secrets"]
    resource_names = [kubernetes_secret.ovh_credentials.metadata[0].name]
    verbs          = ["get", "watch"]
  }
}

resource "kubernetes_cluster_role_binding" "cm_webhook_rb" {
  metadata {
    name = "cert-manager-webhook-ovh:secret-reader"
    # namespace = "cert-manager"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.cm_webhook_role.metadata[0].name
  }
  subject {
    kind      = "ServiceAccount"
    name      = "cert-manager-webhook-ovh"
    namespace = "default"
    api_group = ""
  }
}

resource "kubectl_manifest" "cluster_issuer" {
  yaml_body = templatefile("values/certmanager/issuer.yaml",
    {
      application_key = var.DNS_OVH_APPLICATION_KEY
      consumer_key    = var.DNS_OVH_CONSUMER_KEY
    }
  )

  depends_on = [kubernetes_cluster_role_binding.cm_webhook_rb, helm_release.ovh_webhook]
}

resource "kubectl_manifest" "certificate" {
  yaml_body = file("values/certmanager/cert.yaml")

  depends_on = [kubectl_manifest.cluster_issuer]
}
