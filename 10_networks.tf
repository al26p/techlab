resource "openstack_networking_network_v2" "network" {
  name           = "techlab_net"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "subnet" {
  name            = "techlab_subnet"
  network_id      = openstack_networking_network_v2.network.id
  cidr            = "192.168.169.0/24"
  ip_version      = 4
  dns_nameservers = ["1.1.1.1", "8.8.8.8"]
}

resource "openstack_networking_router_v2" "router_1" {
  name                = "techlab_router"
  admin_state_up      = true
  external_network_id = var.EXT_NET
}


resource "openstack_networking_router_interface_v2" "router_interface_1" {
  router_id = openstack_networking_router_v2.router_1.id
  subnet_id = openstack_networking_subnet_v2.subnet.id
}
