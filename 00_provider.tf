terraform {
  backend "http" {
  }
  required_version = ">= 0.14.0"
  required_providers {
    ovh = {
      source = "ovh/ovh"
    }

    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.51.1"
    }

    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "1.14.0"
    }

    helm = {
      source  = "hashicorp/helm"
      version = "2.11.0"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.23.0"
    }
  }
}

provider "ovh" {
  endpoint           = var.OVH_ENDPOINT
  application_key    = var.OVH_APPLICATION_KEY
  application_secret = var.OVH_APPLICATION_SECRET
  consumer_key       = var.OVH_CONSUMER_KEY
}

provider "openstack" {
  user_name   = var.OS_USERNAME
  tenant_name = var.OS_PROJECT_NAME
  password    = var.OS_PASSWORD
  auth_url    = var.OS_AUTH_URL
  region      = var.OS_REGION
}

provider "helm" {
  kubernetes {
    host                   = ovh_cloud_project_kube.tf_kube_cluster.kubeconfig_attributes[0].host
    cluster_ca_certificate = base64decode(ovh_cloud_project_kube.tf_kube_cluster.kubeconfig_attributes[0].cluster_ca_certificate)
    client_certificate     = base64decode(ovh_cloud_project_kube.tf_kube_cluster.kubeconfig_attributes[0].client_certificate)
    client_key             = base64decode(ovh_cloud_project_kube.tf_kube_cluster.kubeconfig_attributes[0].client_key)
  }
}

provider "kubernetes" {
  host                   = ovh_cloud_project_kube.tf_kube_cluster.kubeconfig_attributes[0].host
  cluster_ca_certificate = base64decode(ovh_cloud_project_kube.tf_kube_cluster.kubeconfig_attributes[0].cluster_ca_certificate)
  client_certificate     = base64decode(ovh_cloud_project_kube.tf_kube_cluster.kubeconfig_attributes[0].client_certificate)
  client_key             = base64decode(ovh_cloud_project_kube.tf_kube_cluster.kubeconfig_attributes[0].client_key)
}

provider "kubectl" {
  host                   = ovh_cloud_project_kube.tf_kube_cluster.kubeconfig_attributes[0].host
  cluster_ca_certificate = base64decode(ovh_cloud_project_kube.tf_kube_cluster.kubeconfig_attributes[0].cluster_ca_certificate)
  client_certificate     = base64decode(ovh_cloud_project_kube.tf_kube_cluster.kubeconfig_attributes[0].client_certificate)
  client_key             = base64decode(ovh_cloud_project_kube.tf_kube_cluster.kubeconfig_attributes[0].client_key)
}
