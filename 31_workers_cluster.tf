# Create an OVHcloud Managed Kubernetes cluster
resource "ovh_cloud_project_kube" "workshop_cluster" {
  count         = var.seats
  service_name  = var.OVH_PROJECT_NAME
  name          = "techlab-workshop-cluster-${count.index}"
  region        = "BHS5"
  version       = "1.27"
  update_policy = "MINIMAL_DOWNTIME"
}

resource "ovh_cloud_project_kube_nodepool" "node_pool_ws" {
  count         = var.seats
  service_name  = var.OVH_PROJECT_NAME
  kube_id       = ovh_cloud_project_kube.workshop_cluster[count.index].id
  name          = "techlabs-ws-pool-${count.index}"
  flavor_name   = "d2-4"
  desired_nodes = 1
  min_nodes     = 1
  max_nodes     = 1

  lifecycle {
    ignore_changes = [desired_nodes]
  }
}

resource "kubernetes_secret" "workshop_kubeconfig" {
  count = var.seats

  metadata {
    name      = "cluster-${count.index}"
    namespace = kubernetes_namespace.editors.metadata[0].name
  }

  data = { config : ovh_cloud_project_kube.workshop_cluster[count.index].kubeconfig }

  depends_on = [ovh_cloud_project_kube.workshop_cluster]
}
