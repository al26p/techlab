variable "seats" {
  type        = number
  description = "Numbers of workspaces to create"
  default     = 0
}

variable "endpoint" {
  type        = string
  description = "URL to use for ide"
  default     = ""
}

variable "OVH_PROJECT_NAME" {
  type        = string
  description = "Public Cloud project name."
}

variable "OVH_ENDPOINT" {
  type        = string
  description = "OVH_ENDPOINT"
  default     = "ovh-eu"
}

variable "OVH_APPLICATION_KEY" {
  type        = string
  description = "OVH_APPLICATION_KEY"
}

variable "OVH_APPLICATION_SECRET" {
  type        = string
  description = "OVH_APPLICATION_SECRET"
}

variable "OVH_CONSUMER_KEY" {
  type        = string
  description = "OVH_CONSUMER_KEY"
}

variable "DNS_OVH_APPLICATION_KEY" {
  type        = string
  description = "OVH_APPLICATION_KEY"
}

variable "DNS_OVH_APPLICATION_SECRET" {
  type        = string
  description = "OVH_APPLICATION_SECRET"
}

variable "DNS_OVH_CONSUMER_KEY" {
  type        = string
  description = "OVH_CONSUMER_KEY"
}

variable "OS_USERNAME" {
  type        = string
  description = "OpenStack Username"
}

variable "OS_PROJECT_NAME" {
  type        = string
  description = "OpenStack Tenant/Project Name"
}

variable "OS_PASSWORD" {
  type        = string
  description = "OpenStack Password"
}

variable "OS_AUTH_URL" {
  type        = string
  description = "OpenStack Identitiy/Keystone API for authentication"
}

variable "OS_REGION" {
  type        = string
  description = "OpenStack Region"
}

variable "EXT_NET" {
  type        = string
  description = "External network id"
}
