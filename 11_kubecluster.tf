# Create an OVHcloud Managed Kubernetes cluster
resource "ovh_cloud_project_kube" "tf_kube_cluster" {
  service_name  = var.OVH_PROJECT_NAME
  name          = "techlab-kube-cluster"
  region        = "BHS5"
  version       = "1.27"
  update_policy = "MINIMAL_DOWNTIME"

  private_network_id = openstack_networking_network_v2.network.id

  private_network_configuration {
    default_vrack_gateway              = openstack_networking_subnet_v2.subnet.gateway_ip
    private_network_routing_as_default = true
  }

  depends_on = [openstack_networking_router_interface_v2.router_interface_1]
}

resource "ovh_cloud_project_kube_nodepool" "node_pool" {
  service_name  = var.OVH_PROJECT_NAME
  kube_id       = ovh_cloud_project_kube.tf_kube_cluster.id
  name          = "techlabs-pool"
  flavor_name   = "b2-7"
  desired_nodes = 1
  max_nodes     = 20
  min_nodes     = 1
  autoscale     = true

  template {
    metadata {
      annotations = {}
      finalizers  = []
      labels = {
        "mks.ovh/version" = "1.27.4-1"
      }
    }
    spec {
      taints        = []
      unschedulable = false
    }
  }
  lifecycle {
    ignore_changes = [desired_nodes]
  }
}
