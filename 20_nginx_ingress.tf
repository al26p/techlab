resource "helm_release" "nginx" {
  name = "ingress-nginx"

  repository       = "https://kubernetes.github.io/ingress-nginx"
  chart            = "ingress-nginx"
  namespace        = "ingress-nginx"
  version          = "4.8.3"
  create_namespace = true

  values = [
    file("values/nginx/values.yaml")
  ]

  depends_on = [ovh_cloud_project_kube_nodepool.node_pool]
}
