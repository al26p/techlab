output "workspaces" {
  description = "Connect to the workspaces !"
  value = [
    for idx in range(var.seats) : {
      host     = "https://ide-${idx}.labs.alban-prats.fr"
      password = "techlab${idx}"
    }
  ]
}
