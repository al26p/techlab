variable "seats" {
  type        = number
  description = "Numbers of workspaces to create"
}

variable "OVH_PROJECT_NAME" {
  type        = string
  description = "Public Cloud project name."
}

variable "OVH_ENDPOINT" {
  type        = string
  description = "OVH_ENDPOINT"
  default     = "ovh-eu"
}

variable "OVH_APPLICATION_KEY" {
  type        = string
  description = "OVH_APPLICATION_KEY"
}

variable "OVH_APPLICATION_SECRET" {
  type        = string
  description = "OVH_APPLICATION_SECRET"
}

variable "OVH_CONSUMER_KEY" {
  type        = string
  description = "OVH_CONSUMER_KEY"
}

