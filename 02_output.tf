output "kubeconfig_file" {
  value     = ovh_cloud_project_kube.tf_kube_cluster.kubeconfig
  sensitive = true
}

output "workspaces" {
  description = "Connect to the workspaces !"
  value = [
    for idx in range(var.seats) : {
      host     = "https://ide-${idx}.labs.workshop.ovh"
      password = "techlab${idx}"
    }
  ]
}
