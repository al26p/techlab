resource "kubernetes_namespace" "editors" {
  metadata {
    name = "editors"
  }
}


resource "kubernetes_deployment" "main" {
  count = var.seats
  #   depends_on = [
  #     kubernetes_persistent_volume_claim.home
  #   ]
  wait_for_rollout = false
  metadata {
    name      = "code-server-${count.index}"
    namespace = kubernetes_namespace.editors.metadata[0].name
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        "app.kubernetes.io/name" = "coder-workspace"
        "instance"               = "coder-workspace-${count.index}"
      }
    }
    strategy {
      type = "Recreate"
    }

    template {
      metadata {
        labels = {
          "app.kubernetes.io/name" = "coder-workspace"
          "instance"               = "coder-workspace-${count.index}"
        }
      }
      spec {
        container {
          name              = "dev"
          image             = "ubuntu:latest"
          image_pull_policy = "Always"
          command = ["sh", "-c", <<-EOT
    set -e
    apt update
    apt install -y curl bash-completion git
    curl -L "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" -o /usr/bin/kubectl
    chmod +x /usr/bin/kubectl
    echo "
    # Customer script
    alias k=kubectl
    source /usr/share/bash-completion/bash_completion
    echo 'source <(kubectl completion bash)' >>~/.bashrc
    echo 'complete -o default -F __start_kubectl k' >>~/.bashrc" >> /root/.bashrc
    cd /usr/bin/
    curl -L https://github.com/derailed/k9s/releases/download/v0.28.2/k9s_Linux_amd64.tar.gz | tar -xz k9s
    curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash


    # install and start code-server
    mkdir -p /root/.config/code-server/
    mkdir -p /root/lab
    cp /root/.kube/config /root/lab/kubeconfig
    mkdir -p /root/User
    echo "{\"workbench.colorTheme\": \"Visual Studio Light\"}" > /root/User/settings.json
    curl -fsSL https://code-server.dev/install.sh | sh
    echo "
    bind-addr: 0.0.0.0:1337
    auth: password
    password: techlab${count.index}
    app-name: "TechLab"
    welcome-text: "Welcome to the TechLab."
    user-data-dir: /root/
    cert: false" > /root/.config/code-server/config.yaml

    cd /root/lab
    git clone https://gitlab.com/al26p/ovh-guides.git
    helm repo add bitnami https://charts.bitnami.com/bitnami

    code-server --install-extension bierner.markdown-preview-github-styles
    code-server --install-extension zaaack.markdown-editor

    code-server --port 13337 /root/lab
  EOT
          ]
          resources {
            requests = {
              "cpu"    = "250m"
              "memory" = "1Gi"
            }
            limits = {
              "cpu"    = "2000m"
              "memory" = "2Gi"
            }
          }
          port {
            container_port = 13337
          }
          volume_mount {
            mount_path = "/root/.kube"
            name       = "kubeconfig"
            read_only  = true
          }

          readiness_probe {
            http_get {
              port = "13337"
              path = "/healthz"
            }
          }
        }

        volume {
          name = "kubeconfig"
          secret {
            secret_name = kubernetes_secret.workshop_kubeconfig[count.index].metadata[0].name
          }
        }

        affinity {
          pod_anti_affinity {
            preferred_during_scheduling_ignored_during_execution {
              weight = 1
              pod_affinity_term {
                topology_key = "kubernetes.io/hostname"
                label_selector {
                  match_expressions {
                    key      = "app.kubernetes.io/name"
                    operator = "In"
                    values   = ["coder-workspace"]
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  depends_on = [kubernetes_secret.workshop_kubeconfig]
}

resource "kubernetes_service" "ide_svc" {
  count = var.seats

  metadata {
    name      = "ide-svc-${count.index}"
    namespace = kubernetes_namespace.editors.metadata[0].name
  }

  spec {
    selector = kubernetes_deployment.main[count.index].spec[0].selector[0].match_labels
    port {
      port        = 13337
      target_port = 13337
    }
    type = "ClusterIP"
  }

  depends_on = [kubernetes_deployment.main]
}


resource "kubectl_manifest" "ide_ingress" {
  count = var.seats

  yaml_body = <<-EOT
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ide-ingress-${count.index}
  namespace: ${kubernetes_namespace.editors.metadata[0].name}
spec:
  ingressClassName: nginx
  rules:
  - host: ide-${count.index}.${var.endpoint}
    http:
      paths:
      - backend:
          service:
            name: ${kubernetes_service.ide_svc[count.index].metadata[0].name}
            port: 
              number: 13337
        path: /
        pathType: Prefix
  tls:
  - hosts:
    - ide-${count.index}.${var.endpoint}
  EOT

  depends_on = [kubernetes_service.ide_svc]
}
